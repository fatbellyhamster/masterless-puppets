

# Development setup

  Pull source from repo to the hypervisor that serves the vagrant boxes.
  Not directly into the vagrant box, because the repo is not public yet.


## On dev machine, get mp bare.

	git clone --bare pan:~/scm/git/masterless-puppets.git masterless-puppets/


## This bare repo functions as a pass-through from the vagrant box to the repo.

	git push --set-upstream origin master

# The rest can be executed in the Vagrantfile.

	wget puppet setup script.
	exec deploy-masterless-puppets.sh
	vagrant ssh

	git clone <vagranthost>:masterless-puppets.git /etc/puppet
