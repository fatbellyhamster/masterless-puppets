#!/bin/bash -e
USER=mnk
HYPERV=192.168.121.1
GIT_STRING="mnk@192.168.121.1:~/scm/git/masterless-puppets"

## Install Git and Puppet
# wget -O /tmp/puppetlabs.deb http://apt.puppetlabs.com/puppetlabs-release-`lsb_release -cs`.deb
# dpkg -i /tmp/puppetlabs.deb
apt-get update
apt-get -y install git-core puppet

# Clone the 'puppet' repo
cd /etc
mv puppet/ puppet-dist
# git clone http://your_git_server_ip/username/puppet.git /etc/puppet
# git clone mnk@192.168.121.1:~/scm/git/masterless-puppets puppet/
git clone $GIT_STRING /etc/puppet

# For development.
rsync -av $USER@$HYPERV:~/.gitconfig ~/

# Initial run to setup the auto-deploy mechanism.
puppet apply /etc/puppet/manifests/site.pp
